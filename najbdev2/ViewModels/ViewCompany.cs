﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace najbdev2.ViewModels
{
    public class ViewCompany
    {
        //User Data

        public int UserID { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
        public string Image2 { get { return FirstName.Replace(" ", string.Empty) + LastName.Replace(" ", string.Empty) + UserID + ".jpg"; } }

        public string UserImage { get; set; }

        //Company Data

        public int CompanyID { get; set; }

        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        public string Address { get; set; }

        public string Industry { get; set; }

        public string Website { get; set; }

        public string ApplyTo { get; set; }
        public int Credits { get; set; }

        public string Description { get; set; }

        [Display(Name = "Upload Image")]
        //[DataType(DataType.ImageUrl)]
        public string Image { get { return CompanyName.Replace(" ", string.Empty) + "CompanyImage" + CompanyID + ".jpg"; } }

        public string CompanyImage { get; set; }

        [Display(Name = "Upload Image")]
        public HttpPostedFileBase File { get; set; }

        //Job Data

        public virtual List<Jobs> Jobs { get; set; }

        [Required]
        [Display(Name = "Job Name")]
        public string JobName { get; set; }

        [Required]
        [Display(Name = "Job Description")]
        public string JobDescription { get; set; }

        [Required]
        [Display(Name = "Job Type")]
        public string JobType { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        public string Date { get; set; }

        [Required]
        [Display(Name = "Location")]
        public string Location { get; set; }

        [Display(Name = "Salary Minimum")]
        public string SalaryMin { get; set; }

        [Display(Name = "Salary Maximum")]
        public string SalaryMax { get; set; }

        //Transaction Data
        public virtual List<Transaction> Transactions { get; set; }

        public int TransactionID { get; set; }
        public string PayPalID { get; set; }
        public int TransactionCompanyID { get; set; }
        public string TransactionCredits { get; set; }
        public string Dollars { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}