﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace najbdev2.ViewModels
{
    public class Jobs
    {
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required]
        public string Location { get; set; }

        [Required]
        public string Industry { get; set; }

        [Required]
        [Display(Name = "Job Type")]
        public string JobType { get; set; }

        [Required]
        [Display(Name = "Active")]
        public bool Active { get; set; }

        public string SalaryMin { get; set; }

        public string SalaryMax { get; set; }

        public Jobs()
        {

        }
        public Jobs(int id, string name, string description, string companyName, string location, string industry, string jobType, bool active, string salaryMin, string salaryMax)
        {
            ID = id;
            Name = name;
            Description = description;
            CompanyName = companyName;
            Location = location;
            Industry = industry;
            JobType = jobType;
            Active = active;
            SalaryMin = salaryMin;
            SalaryMax = salaryMax;
        }
    }
}