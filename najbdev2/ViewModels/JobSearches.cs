﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace najbdev2.ViewModels
{
    public class JobSearches
    {
        public int ID { get; set; }
        public string Location { get; set; }
        public string CompanyName { get; set; }
        public string Industry { get; set; }
        public string JobType { get; set; }
        public string JobName { get; set; }
        public string Radius { get; set; }
        public string SalaryMin { get; set; }
        public string SalaryMax { get; set; }

        public bool Active { get; set; }
        public JobSearches()
        {

        }
        public JobSearches(int id, string location, string companyName, string industry, string jobType, string jobName, string radius, string salaryMin, string salaryMax, bool active)
        {
            ID = id;
            Location = location;
            CompanyName = companyName;
            Industry = industry;
            JobType = jobType;
            JobName = jobName;
            Radius = radius;
            SalaryMin = salaryMin;
            SalaryMax = salaryMax;
            Active = active;
        }
    }
}