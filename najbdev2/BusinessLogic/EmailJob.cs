﻿using Microsoft.AspNet.Identity;
using najbdev2.Models;
using najbdev2.ViewModels;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Net;

namespace najbdev2.BusinessLogic
{
    public class EmailJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            ViewUserRepo.GetJobMatches();

            var to = "";
            var jobMatches = "";

            List<JobMatches> emails = ViewUserRepo.GetEmailList();

            foreach (var email in emails)
            {
                to = email.Email;
                var jobs = email.Jobs;
                foreach (var job in jobs)
                {
                    string image = "http://najbdev.jessebrightmanonline.com/Content/" + job.logo;

                    string jobMatch = "<div style='box-sizing: border-box; padding:25px; border-top: 1px solid rgba(0,0,0,.14); border-bottom: 1px solid rgba(0,0,0,.14);'><img src='" + image + "' alt='image logo' style='display:inline-block; padding:10px;  margin:0px; max-height: 100px; max-width: 120px;'/><p style='display:inline-block; padding:10px; margin:0px;'>Job Name: " + "<a href='http://najbdev.jessebrightmanonline.com/Searcher/Job?id=" + job.jobID.ToString() + "'>" + job.name + "</a></p>" + "<p style='display:inline-block; padding:10px;  margin:0px;'>Company Name: " + job.companyName + "</p>" + "<p style='display:inline-block; padding:10px;  margin:0px;'>Industry: " + job.industry + "</p>" + "<p style='display:inline-block; padding:10px;  margin:0px;'>Location: " + job.location + "</p>" + "<p style='display:inline-block; padding:10px;  margin:0px;'>Job Type: " + job.jobType + "</p><br />" + "<p style='display:inline-block; padding:10px;  margin:0px;'>Description: " + job.description + "</p>" + "<a style='background: 0 0; border: none; border-radius: 2px; color: #000; position: relative; height: 36px; margin: 0; min-width: 64px; padding: 0 16px; display: inline-block; font-family: 'Roboto','Helvetica','Arial',sans-serif; font-size: 14px; font-weight: 500; text-transform: uppercase; letter-spacing: 0; overflow: hidden; transition: box-shadow .2s cubic-bezier(.4,0,1,1),background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1); outline: none; cursor: pointer; text-decoration: none; text-align: center; line-height: 36px; vertical-align: middle; background: rgba(158,158,158,.2); box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12); color: rgb(255,255,255); background-color: rgb(255,82,82);' href='http://najbdev.jessebrightmanonline.com/Searcher/Job?id=" + job.jobID.ToString() + "'>View Job</a>";

                    jobMatches += jobMatch;
                }

                string body = "<div style='box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12); box-sizing: border-box;'><div style='padding:25px; color: black!important; background-color: white!important;'><img src='http://najbdev.jessebrightmanonline.com/Content/images/NAJB_Logo_Web.png' style='height: 50px;'><p class='lead'>The best way to connect job searchers with job hirers.</p></div><div style='padding:25px;'><p><strong>Hello " + to + "!  Here are your job matches for today:</strong></p></div>" + jobMatches + "</div><div style='padding:10px;'><p style='font-size: 10px;'>Copyright NAJB Inc. 2016</p><p style='font-size: 10px;'>All rights reserved.</p></div></div>";

                //using (var message = new MailMessage("info@notajobboard.com", to))
                //{
                //    message.Subject = "NAJB Matches - " + DateTime.Now.ToString("dddd, MMMM dd, yyyy");
                //    message.Body = body;
                //    message.IsBodyHtml = true;

                //        try
                //        {
                //            var emailJob = new EmailJobService();
                //            emailJob.SendAsync(message, to);
                //        }
                //        catch (System.Exception ex)
                //        {
                //            var error = ex.Message;
                //        }

                //}

                using (var message = new MailMessage("info@notajobboard.com", to))
                {
                    message.Subject = "NAJB Matches - " + DateTime.Now.ToString("dddd, MMMM dd, yyyy");
                    message.Body = body;
                    message.IsBodyHtml = true;

                    using (SmtpClient client = new SmtpClient
                    {
                        EnableSsl = false,
                        Host = "mail.jessebrightmanonline.com",
                        Port = 26,
                        Credentials = new NetworkCredential("info@jessebrightmanonline.com", "snowboarding")
                    })
                        try
                        {
                            client.Send(message);
                        }
                        catch (System.Exception ex)
                        {
                            var error = ex.Message;
                        }

                }

                
            }


            //List<JobMatch> emailList = ViewUserRepo.GetEmailList();
            

        }
    }

    //public class EmailJobService
    //{
    //    public Task SendAsync(MailMessage message, string to)
    //    {
    //        // Credentials:
    //        var credentialUserName = "info@jessebrightmanonline.com";
    //        var sentFrom = "info@notajobboard.com";
    //        var pwd = "snowboarding";

    //        // Configure the client:
    //        System.Net.Mail.SmtpClient client =
    //            new System.Net.Mail.SmtpClient("mail.jessebrightmanonline.com");

    //        client.Port = 26;
    //        client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
    //        client.UseDefaultCredentials = false;

    //        // Create the credentials:
    //        System.Net.NetworkCredential credentials =
    //            new System.Net.NetworkCredential(credentialUserName, pwd);

    //        client.EnableSsl = false;
    //        client.Credentials = credentials;

    //        // Create the message:
    //        var mail =
    //            new System.Net.Mail.MailMessage(sentFrom, to);

    //        mail.Subject = message.Subject;
    //        mail.Body = message.Body;
    //        mail.IsBodyHtml = true;

    //        // Send:
    //        return client.SendMailAsync(mail);
    //    }
    //}
}