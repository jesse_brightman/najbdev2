﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace najbdev2.BusinessLogic
{
    public partial class IPN
    {
        public string transactionID { get; set; }
        public Nullable<System.DateTime> txTime { get; set; }
        public string custom { get; set; }
        public string buyerEmail { get; set; }
        public Nullable<decimal> amount { get; set; }
        public string paymentStatus { get; set; }

        public IPN()
        {

        }
    }
}