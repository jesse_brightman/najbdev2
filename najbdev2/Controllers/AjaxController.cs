﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace najbdev2.Controllers
{
    public class AjaxController : Controller
    {
        // GET: Ajax
        najbEntities db = new najbEntities();

        // GET: Ajax
        public ActionResult Index()
        {
            return View();
        }


        //public JsonResult AutoCompleteGetJobName(string term)
        //{
        //    var result = (from r in db.Jobs
        //                  where r.name.ToLower().Contains(term.ToLower())
        //                  select new { name = r.name }).Distinct();
        //    result.OrderBy(r => r.name.Split(','));

        //    var result2 = (from j in db.JobSearches
        //                   where j.jobName.ToLower().Contains(term.ToLower())
        //                   select new { name = j.jobName }).Distinct();
        //    result2.OrderBy(j => j.name.Split(','));

        //    var finalResult = result.Union(result2);

        //    return Json(finalResult, JsonRequestBehavior.AllowGet);
        //}


        public JsonResult AutoCompleteGetJobName(string term)
        {
            var result = (from j in db.JobTitles
                          where j.Title.ToLower().Contains(term.ToLower())
                          select new { j.Title }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AutoCompleteGetCompanyName(string term)
        {
            var result = (from r in db.JobCompanies
                          where r.Companies.ToLower().Contains(term.ToLower())
                          select new { r.Companies }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult AutoCompleteGetJobIndustry(string term)
        //{
        //    var result = (from r in db.Jobs
        //                  where r.industry.ToLower().Contains(term.ToLower())
        //                  select new { r.industry }).Distinct().OrderBy(r => r.industry.Split(','));

        //    var result2 = (from j in db.Jobs
        //                  where j.industry.ToLower().Contains(term.ToLower())
        //                   select new { j.industry }).Distinct().OrderBy(j => j.industry.Split(','));

        //    var finalResult = result.Union(result2);

        //    return Json(finalResult, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult AutoCompleteGetJobIndustry(string term)
        {

            var result = (from j in db.JobIndustries
                          where j.Industries.ToLower().Contains(term.ToLower())
                          select new { j.Industries }).Distinct();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}