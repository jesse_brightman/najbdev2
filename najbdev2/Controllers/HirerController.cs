﻿using najbdev2.BusinessLogic;
using najbdev2.Models;
using najbdev2.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace najbdev2.Controllers
{
    public class HirerController : Controller
    {
        // GET: Hirer
        najbEntities db = new najbEntities();
        HirerRepo repo = new HirerRepo();

        public void getInfo()
        {
            var user = HttpContext.User.Identity.Name;
            ViewBag.Postings = db.Jobs.Where(j => j.Company.NAJB_user.email == user && j.active == true).Count();
        }

        [Authorize(Roles = "Hirer")]
        public ActionResult About()
        {
            var user = HttpContext.User.Identity.Name;
            getInfo();
            return View(repo.GetUserCompany(user));
        }

        [Authorize(Roles = "Hirer")]
        public ActionResult AddCredits()
        {
            var user = HttpContext.User.Identity.Name;
            var userCompany = repo.GetUserCompany(user);
            //ViewUserRepo repo = new ViewUserRepo();
            //repo.GetCompanyByID(id);
            getInfo();
            return View(userCompany);
        }


        [Authorize(Roles = "Hirer")]
        [HttpGet]
        public ActionResult AddJob()
        {
            var user = HttpContext.User.Identity.Name;
            var userCompany = repo.GetUserCompany(user);
            getInfo();
            return View(userCompany);
        }

        [Authorize(Roles = "Hirer")]
        [HttpPost]
        public ActionResult AddJob(ViewCompany company, string email)
        {
            var user = HttpContext.User.Identity.Name;

            if (company.Credits >= 100)
            {
                ViewUserRepo repo = new ViewUserRepo();
                repo.SaveJob(company);

                return RedirectToAction("Hirer");
            }
            else
            {
                @ViewBag.ErrorMessage = "Please add credits in order to post a Job.";
                getInfo();
                return View(repo.GetUserCompany(user));
            }
        }

        [Authorize(Roles = "Hirer")]
        public ActionResult Hirer()
        {

            var user = HttpContext.User.Identity.Name;
            var userCompany = repo.GetUserCompany(user);

            if (userCompany.CompanyName == null)
            {
                return RedirectToAction("AddCompany");
            }
            getInfo();
            return View(userCompany);
        }

        [Authorize(Roles = "Hirer")]
        public ActionResult Archive()
        {
            var user = HttpContext.User.Identity.Name;
            getInfo();
            return View(repo.GetArchive(user));
        }

        [Authorize(Roles = "Hirer")]
        [HttpGet]
        public ActionResult EditHirer()
        {
            var user = HttpContext.User.Identity.Name;
            getInfo();
            return View(repo.GetUserCompany(user));
        }


        [Authorize(Roles = "Hirer")]
        [HttpPost]
        public ActionResult EditHirer(ViewCompany company, string email)
        {
            var user = db.NAJB_user.Where(u => u.email == email).FirstOrDefault();
            company.UserID = user.ID;
            if (company.File != null)
            {
                var filePath = Server.MapPath("/Content/");
                company.File.SaveAs(Path.Combine(filePath, company.Image2));
            }
            ViewUserRepo repo = new ViewUserRepo();
            repo.UpdateCompanyUser(company);
            return RedirectToAction("Hirer");
        }

        [Authorize(Roles = "Hirer")]
        [HttpGet]
        public ActionResult Company()
        {
            var user = HttpContext.User.Identity.Name;
            getInfo();
            return View(repo.GetUserCompany(user));
        }

        [Authorize(Roles = "Hirer")]
        [HttpGet]
        public ActionResult EditCompany()
        {
            var user = HttpContext.User.Identity.Name;
            getInfo();
            return View(repo.GetUserCompany(user));
        }

        [Authorize(Roles = "Hirer")]
        [HttpPost]
        public ActionResult EditCompany(ViewCompany company, string email)
        {
            if (company.File != null)
            {
                var filePath = Server.MapPath("/Content/");
                company.File.SaveAs(Path.Combine(filePath, company.Image));
            }
            HirerRepo repo = new HirerRepo();
            repo.UpdateCompany(company);
            return RedirectToAction("Company", "Hirer");
        }

        [Authorize(Roles = "Hirer")]
        [HttpGet]
        public ActionResult AddCompany()
        {
            var user = HttpContext.User.Identity.Name;
            getInfo();
            return View(repo.GetUserCompany(user));
        }

        [Authorize(Roles = "Hirer")]
        [HttpPost]
        public ActionResult AddCompany(ViewCompany newCompany)
        {
            if (newCompany.File != null)
            {
                var filePath = Server.MapPath("/Content/");
                newCompany.File.SaveAs(Path.Combine(filePath, newCompany.Image));
            }
            ViewUserRepo repo = new ViewUserRepo();
            repo.SaveCompany(newCompany);
            return RedirectToAction("About", "Hirer");
        }

        [Authorize(Roles = "Hirer")]
        public ActionResult ConfirmCredits()
        {
            return RedirectToAction("Hirer");
        }

        


        [Authorize(Roles = "Hirer")]
        public ActionResult DeleteJob(int id)
        {
            var record = db.Jobs.Where(j => j.ID == id).FirstOrDefault();
            if (record != null)
            {
                db.Jobs.Remove(record);
                db.SaveChanges();
            }
            return RedirectToAction("Hirer");
        }

        [Authorize(Roles = "Hirer")]
        public ActionResult ActivateJob(int id)
        {
            ViewUserRepo repo = new ViewUserRepo();
            repo.ActivateJob(id);
            return RedirectToAction("Hirer");
        }

        [Authorize(Roles = "Hirer")]
        public ActionResult DeactivateJob(int id)
        {
            ViewUserRepo repo = new ViewUserRepo();
            repo.InactivateJob(id);
            return RedirectToAction("Hirer");
        }

        [Authorize(Roles = "Hirer")]
        [HttpGet]
        public ActionResult DuplicateJob(int id)
        {
            var user = HttpContext.User.Identity.Name;
            var userCompany = repo.GetUserCompanyDuplicate(user, id);
            getInfo();
            return View(userCompany);
        }

        [Authorize(Roles = "Hirer")]
        [HttpPost]
        public ActionResult DuplicateJob(ViewCompany company, string email)
        {
            var user = HttpContext.User.Identity.Name;

            if (company.Credits >= 100)
            {
                ViewUserRepo repo = new ViewUserRepo();
                repo.SaveJob(company);

                return RedirectToAction("Hirer");
            }
            else
            {
                @ViewBag.ErrorMessage = "Please add credits in order to post a Job.";
                getInfo();
                return View(repo.GetUserCompany(user));
            }
        }
    }
}