﻿using najbdev2.Models;
using najbdev2.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace najbdev2.Controllers
{
    public class SearcherController : Controller
    {
        // GET: Searcher
        najbEntities db = new najbEntities();

        public void getInfo()
        {
            var user = HttpContext.User.Identity.Name;
            ViewBag.Searches = db.JobSearches.Where(j => j.NAJB_user.email == user).Count();
            ViewBag.Favourites = db.JobMatches.Where(j => j.NAJB_user.email == user && j.favourite == true).Count();
        }
        [Authorize(Roles = "Searcher")]

        public ActionResult About()
        {
            var user = HttpContext.User.Identity.Name;
            ViewUserRepo repo = new ViewUserRepo();
            getInfo();
            return View(repo.GetUser(user));
        }

        [Authorize(Roles = "Searcher")]

        public ActionResult Searcher(string message)
        {
            var user = HttpContext.User.Identity.Name;
            ViewUserRepo repo = new ViewUserRepo();
            getInfo();
            return View(repo.GetUser(user));
        }


        [Authorize(Roles = "Searcher")]
        [HttpGet]
        public ActionResult EditSearcher()
        {
            var user = HttpContext.User.Identity.Name;
            ViewUserRepo repo = new ViewUserRepo();
            getInfo();
            return View(repo.GetUser(user));
        }


        [Authorize(Roles = "Searcher")]
        [HttpPost]
        public ActionResult EditSearcher(ViewUser user, string email)
        {
            if (user.File != null)
            {
                var filePath = Server.MapPath("/Content/");
                user.File.SaveAs(Path.Combine(filePath, user.Image));
            }
            ViewUserRepo repo = new ViewUserRepo();
            repo.UpdateUser(user);
            return RedirectToAction("Searcher", "Searcher");
        }

        [Authorize(Roles = "Searcher")]
        [HttpGet]
        public ActionResult AddSearch()
        {
            var user = HttpContext.User.Identity.Name;
            getInfo();
            ViewUserRepo repo = new ViewUserRepo();
            return View(repo.GetUser(user));
        }

        [Authorize(Roles = "Searcher")]
        [HttpPost]
        public ActionResult AddSearch(ViewUser user, string email, string[] locations)
        {
            if (ModelState.IsValid)
            {
                ViewUserRepo repo = new ViewUserRepo();
                repo.SaveJobSearch(user);
                string message = "You have successfully added a Job Search!";
                return RedirectToAction("Searcher", "Searcher", message);
            }

            else
            {
                @ViewBag.ErrorMessage = "";
                return View("Searcher");
            }
        }

        [Authorize(Roles = "Searcher")]
        public ActionResult Favourites()
        {
            var user = HttpContext.User.Identity.Name;
            ViewUserRepo repo = new ViewUserRepo();
            getInfo();
            return View(repo.GetUser(user));
        }

        [Authorize(Roles = "Searcher")]
        public ActionResult DeleteJobSearch(int id)
        {
            var record = db.JobSearches.Where(j => j.ID == id).FirstOrDefault();
            if (record != null)
            {
                db.JobSearches.Remove(record);
                db.SaveChanges();
            }
            return RedirectToAction("Searcher");
        }

        [Authorize(Roles = "Searcher")]
        public ActionResult ActivateJobSearch(int id)
        {
            var user = HttpContext.User.Identity.Name;
            ViewUserRepo repo = new ViewUserRepo();
            repo.ActivateJobSearch(id);
            return RedirectToAction("Searcher");
        }

        [Authorize(Roles = "Searcher")]
        public ActionResult DeactivateJobSearch(int id)
        {
            var user = HttpContext.User.Identity.Name;
            ViewUserRepo repo = new ViewUserRepo();
            repo.DeactivateJobSearch(id);
            return RedirectToAction("Searcher");
        }

        [Authorize(Roles = "Searcher")]
        public ActionResult Deactivate()
        {
            var user = HttpContext.User.Identity.Name;
            ViewUserRepo repo = new ViewUserRepo();
            repo.DeactivateUserSearches(user);
            return View();
        }

        [Authorize(Roles = "Searcher")]
        public ActionResult Job(int id)
        {
            ViewUserRepo repo = new ViewUserRepo();
            var user = HttpContext.User.Identity.Name;
            var ID = id;
            if (User.Identity.IsAuthenticated)
            {
                getInfo();
                return View(repo.GetJob(ID, user));
            }
            else 
            {
                return RedirectToAction("Job", "Home", new { id = ID });
            }
        }

        [Authorize(Roles = "Searcher")]
        public ActionResult Company(int id)
        {
            ViewUserRepo repo = new ViewUserRepo();
            var user = HttpContext.User.Identity.Name;
            var ID = id;
            if (User.Identity.IsAuthenticated)
            {
                getInfo();
                return View(repo.GetCompanyDetails(ID, user));
            }
            else
            {
                return RedirectToAction("Company", "Home", new { id = ID });
            }
        }

        [Authorize(Roles = "Searcher")]
        public ActionResult Favourite(int id)
        {
            ViewUserRepo repo = new ViewUserRepo();
            var user = HttpContext.User.Identity.Name;
            var ID = id;
            repo.SaveFavourite(ID, user);
            return RedirectToAction("Job", new { id = ID });
        }

        [Authorize(Roles = "Searcher")]
        public ActionResult Ignore(int id)
        {
            ViewUserRepo repo = new ViewUserRepo();
            var user = HttpContext.User.Identity.Name;
            var ID = id;
            repo.SaveIgnore(ID, user);
            return RedirectToAction("Job", new { id = ID });
        }

        [Authorize(Roles = "Searcher")]
        public async Task<ActionResult> Share(int id, string shareEmail)
        {
            ViewUserRepo repo = new ViewUserRepo();
            var user = HttpContext.User.Identity.Name;
            var ID = id;
            await repo.SendAsync(id, shareEmail);
            return RedirectToAction("Job", new { id = ID });
        }
    }
}