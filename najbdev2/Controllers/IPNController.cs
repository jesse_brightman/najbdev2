﻿using najbdev2.BusinessLogic;
using najbdev2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace najbdev2.Controllers
{
    public class IPNController : Controller
    {
        // GET: IPN
        //[AllowAnonymous]
        //[ActionName("PayPalIPN")]
        //public ActionResult PayPalIPN()
        //{
        //    var sb = new StringBuilder();
        //    var baseUrl = "https://www.paypal.com/cgi-bin/webscr?cmd=_notify-validate";
        //    var id = "";
        //    var isRefund = false;
        //    var isPaymentCompleted = false;

        //    // build a list of the keys
        //    foreach (var key in GetFormKeys())
        //    {
        //        var value = GetFormValue(key);
        //        var encoded = HttpUtility.UrlEncode(value);

        //        sb.Append("&");
        //        sb.Append(key);
        //        sb.Append("=");
        //        sb.Append(encoded);

        //        if (String.IsNullOrEmpty(key))
        //        {
        //            continue;
        //        }

        //        if (key.Equals("option_selection1", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            // option_selection1=POF8C4AABC
        //            id = value;
        //        }
        //        else if (key.Equals("test_ipn", StringComparison.InvariantCultureIgnoreCase) &&
        //          value == "1")
        //        {
        //            // test_ipn=1
        //            baseUrl = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_notify-validate";
        //        }
        //        else if (key.Equals("reason_code", StringComparison.InvariantCultureIgnoreCase) &&
        //          value == "refund")
        //        {
        //            // reason_code=refund
        //            isRefund = true;
        //        }
        //        else if (key.Equals("payment_status", StringComparison.InvariantCultureIgnoreCase) &&
        //             "Completed".Equals(value, StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            // payment_status=Refunded OR payment_status=Completed
        //            isPaymentCompleted = true;
        //        }
        //    }

        //    var url = baseUrl + sb;
        //    var ipnResponse = ExecuteIpnResponse(url);

        //    if (ipnResponse.Equals("VERIFIED", StringComparison.InvariantCultureIgnoreCase))
        //    {
        //        if (String.IsNullOrEmpty(id))
        //        {
        //            throw new InvalidOperationException("No purchase to match");
        //        }
        //        if (isRefund == false && isPaymentCompleted == false)
        //        {
        //            throw new InvalidOperationException("Should be either RECIEVED or VERIFIED");
        //        }
        //        if (isPaymentCompleted)
        //        {
        //            // todo - ExecutePurchaseComplete(id);
        //        }
        //        if (isRefund)
        //        {
        //            // todo - ExecuteRefundActions(id);
        //        }

        //        // others....?
        //        return new HttpStatusCodeResult(200);
        //    }

        //    throw new InvalidOperationException("paypal ipn failed.");
        //}

        //protected virtual string ExecuteIpnResponse(string url)
        //{
        //    var ipnClient = new WebClient();
        //    var ipnResponse = ipnClient.DownloadString(url);
        //    return ipnResponse;
        //}

        //protected virtual string GetFormValue(string key)
        //{
        //    return Request.Form.Get(key);
        //}

        //protected virtual string[] GetFormKeys()
        //{
        //    return Request.Form.AllKeys;
        //}

        [HttpPost]
        public async Task<ActionResult> Ipn()
        {
            var ipn = Request.Form.AllKeys.ToDictionary(k => k, k => Request[k]);
            ipn.Add("cmd", "_notify-validate");

            var isIpnValid = await ValidateIpnAsync(ipn);
            if (isIpnValid)
            {
                // process the IPN
                IPN customIpn = new IPN();
                customIpn.transactionID = ipn["txn_id"];
                decimal amount = Convert.ToDecimal(ipn["mc_gross"]);
                customIpn.amount = amount;
                customIpn.buyerEmail = ipn["payer_email"];
                customIpn.txTime = DateTime.Now;
                customIpn.custom = ipn["custom"];
                customIpn.paymentStatus = ipn["payment_status"];

                var user = HttpContext.User.Identity.Name.ToString(); ;

                ViewUserRepo repo = new ViewUserRepo();
                repo.AddCredits(customIpn, user);
                return RedirectToAction("Hirer", "Hirer");
            }

            return new EmptyResult();
        }

        private static async Task<bool> ValidateIpnAsync(IEnumerable<KeyValuePair<string, string>> ipn)
        {
            using (var client = new HttpClient())
            {
                const string PayPalUrl = "https://www.sandbox.paypal.com/cgi-bin/webscr";

                // This is necessary in order for PayPal to not resend the IPN.
                await client.PostAsync(PayPalUrl, new StringContent(string.Empty));

                var response = await client.PostAsync(PayPalUrl, new FormUrlEncodedContent(ipn));

                var responseString = await response.Content.ReadAsStringAsync();
                return (responseString == "VERIFIED");
            }
        }
    }
}