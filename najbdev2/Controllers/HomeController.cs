﻿using najbdev2.BusinessLogic;
using najbdev2.Models;
using System;
using System.Web.Mvc;

namespace IdentitySample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
             if (User.IsInRole("Searcher") && User.Identity.IsAuthenticated)
             {
                 return RedirectToAction("Searcher", "Searcher");
             }
             else if(User.IsInRole("Hirer") && User.Identity.IsAuthenticated)
             {
                 return RedirectToAction("Hirer", "Hirer");
             }
            return View();
        }

        public ActionResult Searcher()
        {
            return View();
        }

        public ActionResult Hirer()
        {
            return View();
        }

        [HandleError] 
        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [HandleError] 
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Job(int id)
        {
            ViewUserRepo repo = new ViewUserRepo();
            return View(repo.GetJob(id));
        }

        [HandleError] 
        public ActionResult Company(int id)
        {
            ViewUserRepo viewUserRepo = new ViewUserRepo();
            return View(viewUserRepo.GetCompanyDetails(id));
        }
    }
}
