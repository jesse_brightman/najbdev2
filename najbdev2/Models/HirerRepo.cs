﻿using najbdev2.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace najbdev2.Models
{
    public class HirerRepo
    {
        najbEntities db = new najbEntities();

        public ViewCompany GetUserCompany(string email)
        {
            ViewCompany userCompany = new ViewCompany();
            NAJB_user najbUser = db.NAJB_user.Where(u => u.email == email).FirstOrDefault();

            userCompany.UserID = najbUser.ID;
            userCompany.FirstName = najbUser.firstname;
            userCompany.LastName = najbUser.lastname;
            userCompany.Email = najbUser.email;
            userCompany.UserImage = najbUser.userImage;

            Company company = db.Companies.Where(c => c.NAJB_user.email == email).FirstOrDefault();

            if (company != null)
            {
                userCompany.CompanyID = company.ID;
                userCompany.CompanyName = company.name;
                userCompany.Address = company.address;
                userCompany.Credits = (int)company.credits;
                userCompany.Website = company.website;
                userCompany.ApplyTo = company.applyTo;
                userCompany.Description = company.description;
            }
            else
            {
                userCompany.CompanyName = null;
                userCompany.Jobs = null;
                return userCompany;
            }

            var today = DateTime.Now;
            var jobs = db.Jobs.Where(j => j.Company.name == company.name && (j.jobEndDate > today)).ToList();
            var oldJobs = db.Jobs.Where(j => j.jobEndDate < today).ToList();

            foreach (var oldjob in oldJobs)
            {
                oldjob.active = false;
                try
                {
                    db.SaveChanges();

                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }


            List<Jobs> jobsList = new List<Jobs>();
            foreach (var job in jobs)
            {
                jobsList.Add(new Jobs(job.ID, job.name, job.description, job.Company.name, job.location, job.industry, job.jobType, job.active, job.salaryMin, job.salaryMax));
            }

            userCompany.Jobs = jobsList;
            return userCompany;
        }

        public ViewCompany GetUserCompanyDuplicate(string email, int id)
        {
            ViewCompany userCompany = new ViewCompany();
            NAJB_user najbUser = db.NAJB_user.Where(u => u.email == email).FirstOrDefault();

            userCompany.UserID = najbUser.ID;
            userCompany.FirstName = najbUser.firstname;
            userCompany.LastName = najbUser.lastname;
            userCompany.Email = najbUser.email;
            userCompany.UserImage = najbUser.userImage;

            Company company = db.Companies.Where(c => c.NAJB_user.email == email).FirstOrDefault();

            if (company != null)
            {
                userCompany.CompanyID = company.ID;
                userCompany.CompanyName = company.name;
                userCompany.Address = company.address;
                userCompany.Credits = (int)company.credits;
                userCompany.Website = company.website;
                userCompany.ApplyTo = company.applyTo;
                userCompany.Description = company.description;
            }
            else
            {
                userCompany.CompanyName = null;
                userCompany.Jobs = null;
                return userCompany;
            }

            var job = db.Jobs.Where(j => j.ID == id).FirstOrDefault();
            userCompany.JobName = job.name;
            userCompany.Industry = job.industry;
            userCompany.Location = job.location;
            userCompany.SalaryMin = job.salaryMin;
            userCompany.SalaryMax = job.salaryMax;
            userCompany.Description = job.description;
            userCompany.JobType = job.jobType;
            userCompany.Date = DateTime.Now.ToString();

            return userCompany;
        }

        public bool UpdateCompany(ViewCompany company)
        {
            var thisCompany = db.Companies.Where(c => c.ID == company.CompanyID).FirstOrDefault();

            thisCompany.name = company.CompanyName;
            thisCompany.address = company.Address;
            thisCompany.website = company.Website;
            thisCompany.applyTo = company.ApplyTo;
            thisCompany.description = company.Description;
            thisCompany.logo = company.Image;

            try
            {
                db.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }

            return true;
        }

        public ViewCompany GetArchive(string email)
        {
            ViewCompany userCompany = new ViewCompany();
            NAJB_user najbUser = db.NAJB_user.Where(u => u.email == email).FirstOrDefault();

            userCompany.UserID = najbUser.ID;
            userCompany.FirstName = najbUser.firstname;
            userCompany.LastName = najbUser.lastname;
            userCompany.Email = najbUser.email;
            userCompany.UserImage = najbUser.userImage;

            Company company = db.Companies.Where(c => c.NAJB_user.email == email).FirstOrDefault();

            if (company != null)
            {
                userCompany.CompanyID = company.ID;
                userCompany.CompanyName = company.name;
                userCompany.Address = company.address;
                userCompany.Credits = (int)company.credits;
                userCompany.Website = company.website;
                userCompany.ApplyTo = company.applyTo;
                userCompany.Description = company.description;
            }
            else
            {
                userCompany.CompanyName = null;
                userCompany.Jobs = null;
                return userCompany;
            }

            var jobs = db.Jobs.Where(j => j.Company.name == company.name && j.active == false).ToList();

            List<Jobs> jobsList = new List<Jobs>();
            foreach (var job in jobs)
            {
                jobsList.Add(new Jobs(job.ID, job.name, job.description, job.Company.name, job.location, job.industry, job.jobType, job.active, job.salaryMin, job.salaryMax));
            }

            userCompany.Jobs = jobsList;

            var transactions = db.Transactions.Where(t => t.Company.name == company.name).ToList();

            //List<Transactions> transactionList = new List<Transactions>();
            //foreach (var transaction in transactions)
            //{
            //    transactionList.Add(new Transactions(transaction.CompanyID, transaction.PayPalID.ToString(), transaction.CompanyID, transaction.Credits, transaction.Dollars.ToString(), transaction.TransactionDate));
            //}

            userCompany.Transactions = transactions;

            return userCompany;
        }

        

    }
}