﻿using IdentitySample.Models;
using najbdev2.BusinessLogic;
using najbdev2.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace najbdev2.Models
{
    public class ViewUserRepo
    {
        najbEntities db = new najbEntities();

        public bool SaveMyUser(RegisterViewModel newUser)
        {
            NAJB_user thisUser = new NAJB_user();

            thisUser.email = newUser.Email;
            thisUser.firstname = newUser.FirstName;
            thisUser.lastname = newUser.LastName;
            thisUser.userImage = null;
            thisUser.active = true;
            thisUser.IdentityID = db.AspNetUsers
                             .Where(u => u.UserName == newUser.Email).Select(u => u.Id).FirstOrDefault().ToString();

            AspNetUser user = db.AspNetUsers
                             .Where(u => u.UserName == newUser.Email).FirstOrDefault();
            AspNetRole role = db.AspNetRoles
                             .Where(r => r.Name == newUser.UserType).FirstOrDefault();

            user.AspNetRoles.Add(role);
            db.NAJB_user.Add(thisUser);

            try
            {
                db.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        return false;
                    }
                }
            }
            return true;
        }

        public bool SaveJobSearch(ViewUser newSearchProfile)
        {
            JobSearch thisJobSearch = new JobSearch();

            thisJobSearch.najb_userID = db.NAJB_user.Where(u => u.email == newSearchProfile.Email).Select(u => u.ID).FirstOrDefault();
            thisJobSearch.location = newSearchProfile.Location;
            thisJobSearch.companyName = newSearchProfile.CompanyName;
            thisJobSearch.industry = newSearchProfile.Industry;
            thisJobSearch.jobType = newSearchProfile.JobType;
            thisJobSearch.jobName = newSearchProfile.JobName;
            thisJobSearch.radius = newSearchProfile.Radius;
            thisJobSearch.salaryMin = newSearchProfile.SalaryMin;
            thisJobSearch.salaryMax = newSearchProfile.SalaryMax;
            thisJobSearch.additionalLocations = newSearchProfile.AdditionalLocations;
            thisJobSearch.active = true;

            db.JobSearches.Add(thisJobSearch);

            db.SaveChanges();

            var jobNames = db.Jobs.ToList();
            var jobTitles = db.JobTitles.ToList();

            foreach (var jobName in jobNames)
            {
                foreach (var jobTitle in jobTitles)
                {
                    if(jobName.name.ToLower().Contains(jobTitle.Title.ToLower()))
                    {
                        
                    }
                    else
                    {
                        JobTitle newJobTitle = new JobTitle();
                        newJobTitle.Title = jobName.name;

                        db.JobTitles.Add(newJobTitle);
                        db.SaveChanges();
                    }
                }
            }

            var jobIndustries = db.Jobs.ToList();
            var jobIndustriesNames = db.JobIndustries.ToList();

            foreach (var jobIndustry in jobIndustries)
            {
                foreach (var jobIndustriesName in jobIndustriesNames)
                {
                    if (jobIndustry.industry.ToLower().Contains(jobIndustriesName.Industries.ToLower()))
                    {
                        
                    }
                    else
                    {
                        JobIndustry newJobIndustry = new JobIndustry();
                        newJobIndustry.Industries = jobIndustry.industry;

                        db.JobIndustries.Add(newJobIndustry);
                        db.SaveChanges();
                    }
                }
            }

            var jobCompanies = db.Jobs.ToList();
            var jobCompanyNames = db.JobCompanies.ToList();

            foreach (var jobCompany in jobCompanies)
            {
                foreach (var jobCompanyName in jobCompanyNames)
                {
                    if (jobCompany.name.ToLower().Contains(jobCompanyName.Companies.ToLower()))
                    {

                    }
                    else
                    {
                        JobCompany newJobCompany = new JobCompany();
                        newJobCompany.Companies = jobCompany.name;

                        db.JobCompanies.Add(newJobCompany);
                        db.SaveChanges();
                    }
                }
            }
            return true;
        }
        public bool SaveUser(ViewUser user)
        {
            NAJB_user najbUser = db.NAJB_user.Where(u => u.email == user.Email).FirstOrDefault();

            najbUser.firstname = user.FirstName;
            najbUser.lastname = user.LastName;
            najbUser.email = user.Email;
            najbUser.userImage = user.Image;

            db.NAJB_user.Add(najbUser);

            if (user.CompanyName != null || user.Industry != null || user.JobType != null || user.Location != null || user.JobName != null)
            {
                JobSearch thisJobSearch = new JobSearch();

                thisJobSearch.NAJB_user.firstname = user.FirstName;
                thisJobSearch.NAJB_user.lastname = user.LastName;
                thisJobSearch.NAJB_user.email = user.Email;
                thisJobSearch.location = user.Location;
                thisJobSearch.industry = user.Industry;
                thisJobSearch.jobType = user.JobType;
                thisJobSearch.jobName = user.JobName;
                thisJobSearch.radius = user.Radius;
                thisJobSearch.salaryMin = user.SalaryMin;
                thisJobSearch.salaryMax = user.SalaryMax;

                db.JobSearches.Add(thisJobSearch);
            }
            db.SaveChanges();
            return true;
        }

        public bool UpdateUser(ViewUser user)
        {
            NAJB_user najbUser = db.NAJB_user.Where(u => u.email == user.Email).FirstOrDefault();

            najbUser.firstname = user.FirstName;
            najbUser.lastname = user.LastName;
            najbUser.email = user.Email;
            najbUser.userImage = user.Image;

            try
            {
                db.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }

            return true;
        }

        public bool UpdateCompanyUser(ViewCompany company)
        {

            // change to company and user
            NAJB_user najbUser = db.NAJB_user.Where(u => u.email == company.Email).FirstOrDefault();

            najbUser.firstname = company.FirstName;
            najbUser.lastname = company.LastName;
            najbUser.email = company.Email;
            najbUser.userImage = company.Image2;

            try
            {
                db.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }

            return true;
        }

        //public bool ValidLogin(Login login)
        //{
        //    UserStore<IdentityUser> userStore = new UserStore<IdentityUser>();
        //    UserManager<IdentityUser> userManager = new UserManager<IdentityUser>(userStore)
        //    {
        //        UserLockoutEnabledByDefault = true,
        //        DefaultAccountLockoutTimeSpan = new TimeSpan(0, 10, 0),
        //        MaxFailedAccessAttemptsBeforeLockout = 3
        //    };
        //    var user = userManager.FindByName(login.UserName);

        //    if (user == null)
        //        return false;

        //    // User is locked out.
        //    if (userManager.SupportsUserLockout && userManager.IsLockedOut(user.Id))
        //        return false;

        //    // Validated user was locked out but now can be reset.
        //    if (userManager.CheckPassword(user, login.Password)
        //        && userManager.IsEmailConfirmed(user.Id))
        //    {
        //        if (userManager.SupportsUserLockout
        //         && userManager.GetAccessFailedCount(user.Id) > 0)
        //        {
        //            userManager.ResetAccessFailedCount(user.Id);
        //        }
        //    }
        //    // Login is invalid so increment failed attempts.
        //    else
        //    {
        //        bool lockoutEnabled = userManager.GetLockoutEnabled(user.Id);
        //        if (userManager.SupportsUserLockout && userManager.GetLockoutEnabled(user.Id))
        //        {
        //            userManager.AccessFailed(user.Id);
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        public const string EMAIL_CONFIRMATION = "EmailConfirmation";
        public const string PASSWORD_RESET = "ResetPassword";
        //public bool CreateTokenProvider(UserManager<IdentityUser> manager, string tokenType)
        //{
        //    var provider = new DpapiDataProtectionProvider("MyApplicaitonName");
        //    manager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(
        //    provider.Create(tokenType));
        //    return true;
        //}

        public Job GetJob(int id)
        {
            var job = db.Jobs.Where(j => j.ID == id).FirstOrDefault();

            return job;
        }
        public ViewUser GetJob(int id, string email)
        {
            ViewUser user = new ViewUser();
            NAJB_user najbUser = db.NAJB_user.Where(u => u.email == email).FirstOrDefault();

            user.UserID = najbUser.ID;
            user.FirstName = najbUser.firstname;
            user.LastName = najbUser.lastname;
            user.Email = najbUser.email;
            user.UserImage = najbUser.userImage;

            var job = db.Jobs.Where(j => j.ID == id).FirstOrDefault();
            user.Job = job;

            return user;
        }

        public IEnumerable<Job> GetJobs()
        {
            var jobs = db.Jobs.ToList();
            return jobs;
        }

        public bool SaveJob(ViewCompany newJob)
        {
            Job job = new Job();

            job.companyID = db.Companies.Where(c => c.NAJB_user.email == HttpContext.Current.User.Identity.Name).Select(u => u.ID).FirstOrDefault();
            job.description = newJob.JobDescription;
            job.location = newJob.Location;
            job.name = newJob.JobName;
            job.jobType = newJob.JobType;
            job.industry = newJob.Industry;
            job.salaryMin = newJob.SalaryMin;
            job.salaryMax = newJob.SalaryMax;
            job.jobDate = DateTime.Now;
            job.jobEndDate = job.jobDate.AddDays(60);
            job.active = true;

            db.Jobs.Add(job);

            var company = db.Companies.Where(c => c.ID == newJob.CompanyID).FirstOrDefault();
            company.credits -= 100;

            try
            {
                db.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }

            var jobNames = db.Jobs.ToList();
            var jobTitles = db.JobTitles.ToList();

            foreach (var jobName in jobNames)
            {
                foreach (var jobTitle in jobTitles)
                {
                    if (jobName.name.ToLower().Contains(jobTitle.Title.ToLower()))
                    {

                    }
                    else
                    {
                        JobTitle newJobTitle = new JobTitle();
                        newJobTitle.Title = newJob.JobName;

                        db.JobTitles.Add(newJobTitle);
                        db.SaveChanges();
                    }
                }
            }

            var jobIndustries = db.Jobs.ToList();
            var jobIndustriesNames = db.JobIndustries.ToList();

            foreach (var jobIndustry in jobIndustries)
            {
                foreach (var jobIndustriesName in jobIndustriesNames)
                {
                    if (jobIndustry.industry.ToLower().Contains(jobIndustriesName.Industries.ToLower()))
                    {

                    }
                    else
                    {
                        JobIndustry newJobIndustry = new JobIndustry();
                        newJobIndustry.Industries = newJob.Industry;

                        db.JobIndustries.Add(newJobIndustry);
                        db.SaveChanges();
                    }
                }
            }

            var jobCompanies = db.Jobs.ToList();
            var jobCompanyNames = db.JobCompanies.ToList();

            foreach (var jobCompany in jobCompanies)
            {
                foreach (var jobCompanyName in jobCompanyNames)
                {
                    if (jobCompany.name.ToLower().Contains(jobCompanyName.Companies.ToLower()))
                    {

                    }
                    else
                    {
                        JobCompany newJobCompany = new JobCompany();
                        newJobCompany.Companies = newJob.CompanyName;

                        db.JobCompanies.Add(newJobCompany);
                        db.SaveChanges();
                    }
                }
            }

            return true;
        }

        public bool SaveCompany(ViewCompany newCompany)
        {
            Company company = new Company();

            company.name = newCompany.CompanyName;
            company.address = newCompany.Address;
            company.logo = newCompany.Image;
            company.NAJB_userID = db.NAJB_user.Where(u => u.email == HttpContext.Current.User.Identity.Name).Select(u => u.ID).FirstOrDefault();
            company.description = newCompany.Description;
            company.credits = 0;
            company.website = newCompany.Website;
            company.applyTo = newCompany.ApplyTo;

            db.Companies.Add(company);
            db.SaveChanges();

            var companyNames = db.Companies.ToList();
            var jobCompanyNames = db.JobCompanies.ToList();

            foreach (var companyName in companyNames)
            {
                foreach (var jobCompanyName in jobCompanyNames)
                {
                    if (companyName.name.ToLower().Contains(jobCompanyName.Companies.ToLower()))
                    {
                        continue;
                    }
                    else
                    {
                        JobCompany newJobCompanyName = new JobCompany();
                        newJobCompanyName.Companies = companyName.name;

                        db.JobCompanies.Add(newJobCompanyName);
                        db.SaveChanges();
                    }
                }
            }

            return true;
        }

        public Company GetCompany(string userName)
        {
            var company = db.Companies.Where(c => c.NAJB_user.email == userName).FirstOrDefault();

            return company;
        }

        public Company GetCompanyDetails(int id)
        {
            var company = db.Companies.Where(c => c.ID == id).FirstOrDefault();

            return company;
        }

        public ViewUser GetCompanyDetails(int id, string email)
        {
            ViewUser user = new ViewUser();
            NAJB_user najbUser = db.NAJB_user.Where(u => u.email == email).FirstOrDefault();

            user.UserID = najbUser.ID;
            user.FirstName = najbUser.firstname;
            user.LastName = najbUser.lastname;
            user.Email = najbUser.email;
            user.UserImage = najbUser.userImage;

            var company = db.Companies.Where(c => c.ID == id).FirstOrDefault();
            user.Company = company;

            return user;
        }

        public static bool GetJobMatches()
        {
            najbEntities db = new najbEntities();
            IEnumerable<Job> jobs = db.Jobs.Where(j => j.active == true).ToList();
            IEnumerable<JobSearch> jobSearches = db.JobSearches.Where(j => j.active == true).ToList();

            foreach (var jobSearch in jobSearches)
            {
                var nullCount = 0;

                foreach (var job in jobs)
                {
                    //check matches on location
                    if ((jobSearch.location == null || job.location.ToLower().Contains(jobSearch.location.ToLower())))
                    {
                        if (jobSearch.location == null)
                        {
                            nullCount = nullCount + 1;
                        }
                        if ((jobSearch.additionalLocations == null || jobSearch.additionalLocations.ToLower().Contains(job.location.ToLower())))
                        {
                            if ((jobSearch.industry == null || job.industry.ToLower().Contains(jobSearch.industry.ToLower()) || jobSearch.industry.ToLower().Contains(job.industry.ToLower())))
                            {
                                if (jobSearch.industry == null)
                                {
                                    nullCount = nullCount + 1;
                                }
                                if ((jobSearch.jobType == null || job.jobType.ToLower().Contains(jobSearch.jobType.ToLower())))
                                {
                                    if (jobSearch.jobType == null)
                                    {
                                        nullCount = nullCount + 1;
                                    }
                                    if ((jobSearch.companyName == null || job.Company.name.ToLower().Contains(jobSearch.companyName.ToLower())))
                                    {
                                        if (jobSearch.companyName == null)
                                        {
                                            nullCount = nullCount + 1;
                                        }
                                        if ((jobSearch.jobName == null || job.name.ToLower().Contains(jobSearch.jobName.ToLower())))
                                        {
                                            if (jobSearch.jobName == null)
                                            {
                                                nullCount = nullCount + 1;
                                            }

                                            if (nullCount <= 3)
                                            {
                                                var jobMatch = new JobMatch();
                                                
                                                jobMatch.email = jobSearch.NAJB_user.email;
                                                jobMatch.firstname = jobSearch.NAJB_user.firstname;
                                                jobMatch.lastname = jobSearch.NAJB_user.lastname;
                                                jobMatch.location = job.location;
                                                jobMatch.industry = jobSearch.industry;
                                                jobMatch.jobType = jobSearch.jobType;
                                                jobMatch.name = job.name;
                                                jobMatch.description = job.description;
                                                jobMatch.companyName = job.Company.name;
                                                jobMatch.jobID = (int)job.ID;
                                                jobMatch.active = true;
                                                jobMatch.favourite = false;
                                                jobMatch.ignore = false;
                                                jobMatch.NAJB_userID = (int)jobSearch.najb_userID;
                                                jobMatch.companyID = job.companyID;

                                                var jobMatches = db.JobMatches.ToList();

                                                if(!jobMatches.Contains(jobMatch))
                                                {
                                                    db.JobMatches.Add(jobMatch);

                                                    try
                                                    {
                                                        db.SaveChanges();
                                                        return true;
                                                    }
                                                    catch (DbEntityValidationException dbEx)
                                                    {
                                                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                                                        {
                                                            foreach (var validationError in validationErrors.ValidationErrors)
                                                            {
                                                                System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName,                                  validationError.ErrorMessage);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    break;
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }
        public static List<JobMatches> GetEmailList()
        {
            najbEntities db = new najbEntities();

            // Creates a TextInfo based on the "en-US" culture.
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;

            List<JobMatches> jobMatchesList = new List<JobMatches>();
            JobMatches jobMatches = new JobMatches();

            var emails = db.JobMatches.Select(e => e.email).ToList();
            var distinct = emails.Distinct().ToList();

            foreach (var email in distinct)
            {
                jobMatches.Email = email;

                List<JobMatch> jobMatchList = new List<JobMatch>();
                var thisJobMatchList = db.JobMatches.Where(j => j.email == email && j.ignore != true).ToList();

                foreach (var job in thisJobMatchList)
                {
                    JobMatch jobMatch = new JobMatch();
                    {
                        jobMatch.companyName = myTI.ToTitleCase(job.companyName);
                        jobMatch.description = job.description;
                        jobMatch.industry = myTI.ToTitleCase(job.industry);
                        jobMatch.name = myTI.ToTitleCase(job.name);
                        jobMatch.location = myTI.ToTitleCase(job.location);
                        jobMatch.jobType = job.jobType;
                        jobMatch.salaryMin = job.salaryMin;
                        jobMatch.salaryMax = job.salaryMax;
                        jobMatch.logo = job.companyName.Replace(" ", "") + "CompanyImage" + job.companyID + ".jpg";
                        jobMatch.jobID = job.jobID;
                    }
                    jobMatchList.Add(jobMatch);
                }
                jobMatches.Jobs = jobMatchList;
            }
            jobMatchesList.Add(jobMatches);
            return jobMatchesList;
        }

        public static bool ClearJobMatches()
        {
            najbEntities db = new najbEntities();
            //db.Database.ExecuteSqlCommand("TRUNCATE TABLE [JobMatches]");
            var jobMatches = db.JobMatches.ToList();
            foreach (var jobMatch in jobMatches)
            {
                jobMatch.active = false;
            }
            db.SaveChanges();
            return true;
        }
        public ViewUser GetUser(string email)
        {
            ViewUser user = new ViewUser();
            NAJB_user najbUser = db.NAJB_user.Where(u => u.email == email).FirstOrDefault();

            user.UserID = najbUser.ID;
            user.FirstName = najbUser.firstname;
            user.LastName = najbUser.lastname;
            user.Email = najbUser.email;
            user.UserImage = najbUser.userImage;

            var jobSearches = db.JobSearches.Where(j => j.NAJB_user.email == email).ToList();

            List<JobSearches> jobs = new List<JobSearches>();
            foreach (var job in jobSearches)
            {
                jobs.Add(new JobSearches(job.ID, job.location, job.companyName, job.industry, job.jobType, job.jobName, job.radius, job.salaryMin, job.salaryMax, job.active));
            }

            user.JobSearches = jobs;

            var jobMatches = db.JobMatches.Where(j => j.NAJB_user.email == email && j.favourite == true).ToList();

            List<JobMatchFavourites> matches = new List<JobMatchFavourites>();
            foreach (var match in jobMatches)
            {
                matches.Add(new JobMatchFavourites(match.ID, match.location, match.companyName, match.industry, match.jobType, match.name, match.salaryMin, match.salaryMax, (bool)match.active, (bool)match.favourite));
            }

            user.JobMatchFavourites = matches;

            return user;
        }

        public ViewCompany GetUserCompany(string email)
        {
            ViewCompany userCompany = new ViewCompany();
            NAJB_user najbUser = db.NAJB_user.Where(u => u.email == email).FirstOrDefault();

            userCompany.UserID = najbUser.ID;
            userCompany.FirstName = najbUser.firstname;
            userCompany.LastName = najbUser.lastname;
            userCompany.Email = najbUser.email;
            userCompany.UserImage = najbUser.userImage;

            Company company = db.Companies.Where(c => c.NAJB_user.email == email).FirstOrDefault();

            if (company != null)
            {
                userCompany.CompanyID = company.ID;
                userCompany.CompanyName = company.name;
                userCompany.Credits = (int)company.credits;
                userCompany.Website = company.website;
                userCompany.Description = company.description;
            }
            else
            {
                userCompany.CompanyName = null;
                userCompany.Jobs = null;
                return userCompany;
            }

            var today = DateTime.Now;
            var jobs = db.Jobs.Where(j => j.Company.name == company.name && (j.jobEndDate < today)).ToList();
            var oldJobs = db.Jobs.Where(j => j.jobEndDate > today).ToList();

            foreach (var oldjob in oldJobs)
            {
                oldjob.active = false;
                try
                {
                    db.SaveChanges();

                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }


            List<Jobs> jobsList = new List<Jobs>();
            foreach (var job in jobs)
            {
                jobsList.Add(new Jobs(job.ID, job.name, job.description, job.Company.name, job.location, job.industry, job.jobType, job.active, job.salaryMin, job.salaryMax));
            }

            userCompany.Jobs = jobsList;
            return userCompany;
        }



        public bool DeactivateUserSearches(string email)
        {
            var user = db.NAJB_user.Where(u => u.email == email).FirstOrDefault();

            var searches = db.JobSearches.Where(s => s.najb_userID == user.ID).ToList();

            foreach (var search in searches)
            {
                search.active = false;
            }

            try
            {
                db.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            return true;
        }

        public bool ActivateJobSearch(int id)
        {
            var jobSearch = db.JobSearches.Where(j => j.ID == id).FirstOrDefault();
            jobSearch.active = true;

            try
            {
                db.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        return false;
                    }
                }
            }

            return true;
        }

        public bool DeactivateJobSearch(int id)
        {
            var jobSearch = db.JobSearches.Where(j => j.ID == id).FirstOrDefault();
            jobSearch.active = false;

            try
            {
                db.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        return false;
                    }
                }
            }

            return true;
        }

        public bool AddCredits(IPN ipn, string email)
        {
            var company = db.Companies.Where(c => c.NAJB_user.email == email).FirstOrDefault();

            if(company.Transactions.Count() == 0)
            {
                company.credits += (2*(Int32.Parse(ipn.custom)));
            }
            else 
            {
                company.credits += Int32.Parse(ipn.custom);
            }
            

            db.SaveChanges();

            if(AddTransaction(ipn, email))
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public bool ActivateJob(int id)
        {
            var activate = db.Jobs.Where(j => j.ID == id).FirstOrDefault();

            activate.active = true;

            db.SaveChanges();

            return true;
        }

        public bool InactivateJob(int id)
        {
            var activate = db.Jobs.Where(j => j.ID == id).FirstOrDefault();

            activate.active = false;

            db.SaveChanges();

            return true;
        }

        public Company GetCompanyByID(int id)
        {
            var company = db.Companies.Where(c => c.ID == id).FirstOrDefault();

            return company;
        }

        public bool AddTransaction(IPN ipn, string email)
        {
            Transaction transaction = new Transaction();

            transaction.PayPalID = ipn.transactionID;
            transaction.CompanyID = db.Companies.Where(c => c.NAJB_user.email == email).FirstOrDefault().ID;
            transaction.Credits = Int32.Parse(ipn.custom);
            transaction.Dollars = (decimal)ipn.amount;
            transaction.TransactionDate = DateTime.Now;

            db.Transactions.Add(transaction);
            db.SaveChanges();

            return true;
        }

        public bool SaveFavourite(int id, string email)
        {
            var favourite = db.JobMatches.Where(j => j.jobID == id && j.NAJB_user.email == email).FirstOrDefault();
            favourite.favourite = true;

            db.SaveChanges();

            return true;
        }

        public bool SaveIgnore(int id, string email)
        {
            var ignore = db.JobMatches.Where(j => j.jobID == id && j.NAJB_user.email == email).FirstOrDefault();
            ignore.ignore = true;

            db.SaveChanges();

            return true;
        }


        public Task SendAsync(int id, string email)
        {
            var job = db.Jobs.Where(j => j.ID == id).FirstOrDefault();
            // Credentials:
            var credentialUserName = "info@jessebrightmanonline.com";
            var sentFrom = "info@notajobboard.com";
            var pwd = "snowboarding";

            // Configure the client:
            System.Net.Mail.SmtpClient client =
                new System.Net.Mail.SmtpClient("mail.jessebrightmanonline.com");

            client.Port = 26;
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;

            // Create the credentials:
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(credentialUserName, pwd);

            client.EnableSsl = false;
            client.Credentials = credentials;

            // Create the message:
            var mail =
                new System.Net.Mail.MailMessage(sentFrom, email);
            var image = "http://najbdev.jessebrightmanonline.com/Content/" + job.Company.logo;
            mail.Subject = "A friend has shared a NAJB job with you.";
            mail.Body = "<div style='box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12); box-sizing: border-box;'><div style='padding:25px; color: black!important; background-color: white!important;'><img src='http://najbdev.jessebrightmanonline.com/Content/images/NAJB_Logo_Web.png' style='height: 50px;'><p class='lead'>The best way to connect job searchers with job hirers.</p></div><div style='padding:25px;'><p><strong>Hello " + email + "!  A friend wants you to look at this job:</strong></p></div><div style='box-sizing: border-box; padding:25px; border-top: 1px solid rgba(0,0,0,.14); border-bottom: 1px solid rgba(0,0,0,.14);'><img src='" + image + "' alt='image logo' style='display:inline-block; padding:10px;  margin:0px; max-height: 100px; max-width: 120px;'/><p style='display:inline-block; padding:10px; margin:0px;'>Job Name: " + "<a href='http://najbdev.jessebrightmanonline.com/Home/Job?id=" + job.ID.ToString() + "'>" + job.name + "</a></p>" + "<p style='display:inline-block; padding:10px;  margin:0px;'>Company Name: " + job.Company.name + "</p>" + "<p style='display:inline-block; padding:10px;  margin:0px;'>Industry: " + job.industry + "</p>" + "<p style='display:inline-block; padding:10px;  margin:0px;'>Location: " + job.location + "</p>" + "<p style='display:inline-block; padding:10px;  margin:0px;'>Job Type: " + job.jobType + "</p><br />" + "<p style='display:inline-block; padding:10px;  margin:0px;'>Description: " + job.description + "</p>" + "<a style='background: 0 0; border: none; border-radius: 2px; color: #000; position: relative; height: 36px; margin: 0; min-width: 64px; padding: 0 16px; display: inline-block; font-family: 'Roboto','Helvetica','Arial',sans-serif; font-size: 14px; font-weight: 500; text-transform: uppercase; letter-spacing: 0; overflow: hidden; transition: box-shadow .2s cubic-bezier(.4,0,1,1),background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1); outline: none; cursor: pointer; text-decoration: none; text-align: center; line-height: 36px; vertical-align: middle; background: rgba(158,158,158,.2); box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12); color: rgb(255,255,255); background-color: rgb(255,82,82);' href='http://najbdev.jessebrightmanonline.com/Home/Job?id=" + job.ID.ToString() + "'>View Job</a></div><div style='padding:10px;'><p style='font-size: 10px;'>Copyright NAJB Inc. 2016</p><p style='font-size: 10px;'>All rights reserved.</p></div></div>";
            mail.IsBodyHtml = true;

            // Send:
            return client.SendMailAsync(mail);
        }
    }
}